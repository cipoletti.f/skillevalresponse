# SkillEval

Recordá que tenés un tiempo límite para resolverlo y también de informar cuando hayas concluído.

## Let Go!

Esta evaluación no busca un resultado esperado, no pretende tampoco que se considere completo o incompleto. Busca resolver todo aquello que comprendas y puedas hacer.
Todos los resultados deben estar presentados de modo tal de poder ejecutarlos con tan solo copypastear sus líneas.
