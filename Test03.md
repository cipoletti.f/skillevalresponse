# ALGORITMOS
Escribir una función simple (no más de 100 caracteres) que retorne un boolean indicando si un string
dado es un palíndromo.


RESOLUCION (Mayor a 100 chars):

function f(s){var p=true,w=s.length,l=w/2;
for (var i=0;i<l;i++){if(s[i]===s[(w-i-1)]){}else{p=false;}
}console.log(p)}
f('')

function
# JAVASCRIPT
What will the code below output to the console and why?
var myObject = {
    foo : "bar",
    func: function() {
        var self=this;
        console.log("outer func: this.foo = "+this.foo);
        console.log("outer func: self.foo = "+self.foo);
        (function(){
            console.log("inner func: this.foo = " +this.foo);
            console.log("inner func: self.foo = " +self.foo);   
        }());
    }
};

myObject.func();
1. ¿What is a Promise?
2. ¿What is ECMAScript?
3. ¿What is NaN? What is its type? How can you reliably test if a value is equal to NaN ?

    Es el dato que devuelto por funciones que no pueden parsear el dato a valor numerico.
    Se utiliza isNaN().

4. ¿What will be the output when the following code is executed? Explain.
console.log(false=='0') 

    True

console.log(false==='0')

    False

La triple igualdad compara el tipo de dato, en cambio el doble igual no.

# PHP
1. ¿Cuál es la diferencia entre las funciones include() y require()?
2. ¿Cuál es la diferencia entre GET y POST?

    GET > Metodo que se utiliza para obtener datos desde una API REST
    POST> Metodo que se utiliza para publicar datos desde una API REST

3. ¿Cómo se puede habilitar el reporte de errores en PHP?
4. ¿Cuáles son los métodos __construct() y __destruct() en una clase PHP?
