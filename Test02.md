# CASO 1
Dada las siguientes funciones
  var Foo = function( a ) {
    var baz = function() {
      return a;
    };
    this.baz = function() {
      return a;
    };
  };

  Foo.prototype = {
    biz: function() {
      return a;
    }
  };

  var f = new Foo( 7 );
  f.bar(); 
  f.baz(); 
  f.biz(); 


1. ¿Cuál es el resultado de las últimas 3 líneas?
  f.bar(); => No esta definida
  f.baz(); => Devuelve 7
  f.biz();  => Error de referencia, el parametro a no esta definido.

2. ¿Modificá el código de f.bar() para que retorne 7?

RESOLUCION:

  var Foo = function( a ) {
    var baz = function() {
      return a;
    };
    this.baz = function() {
      return a;
    };
    
    this.bar = function() {
      return a;
    };
  };

  Foo.prototype = {
    biz: function() {
      return a;
    }
  };

  var f = new Foo( 7 );
  //f.bar(); 
console.log(f.bar()); 
  //f.biz(); 
3. ¿Modificá el código para que f.biz() también retorne 7?

RESOLUCION:

  var Foo = function( a ) {
  this.a = a;
    var baz = function() {
      return a;
    };
    this.baz = function() {
      return a;
    };
    
    this.bar = function() {
      return a;
      
    };
  };

  Foo.prototype = {
  
    biz: function() {
      return this.a;
    }
  };

  var f = new Foo( 7 );
//console.log(f.bar()); 
//console.log(f.baz()); 
console.log(f.biz());

# CASO 2
Partiendo del siguiente array:
var endorsements = [
  { skill: 'css', user: 'Bill' },
  { skill: 'javascript', user: 'Chad' },
  { skill: 'javascript', user: 'Bill' },
  { skill: 'css', user: 'Sue' },
  { skill: 'javascript', user: 'Sue' },
  { skill: 'html', user: 'Sue' }
];
1. ¿Cómo podrías ordenarlo de la siguiente forma?:
[
  { skill: 'css', users: [ 'Bill', 'Sue', 'Sue' ], count: 2 },
  { skill: 'javascript', users: [ 'Chad', 'Bill', 'Sue' ], count: 3 },
  { skill: 'html', users: [ 'Sue' ], count: 1 }
]


RESOLUCION:

var endorsements = [
  { skill: 'css', user: 'Bill' },
  { skill: 'javascript', user: 'Chad' },
  { skill: 'javascript', user: 'Bill' },
  { skill: 'css', user: 'Sue' },
  { skill: 'javascript', user: 'Sue' },
  { skill: 'html', user: 'Sue' }
];

//Creamos un nuevo objeto donde vamos a almacenar por ciudades. 
let nuevoObjeto = {}
endorsements.forEach( x => {
  //Si la ciudad no existe en nuevoObjeto entonces
  //la creamos e inicializamos el arreglo de profesionales. 
  if( !nuevoObjeto.hasOwnProperty(x.skill)){
    nuevoObjeto[x.skill] = {
      users: [],
      count: 0
    }
  }
  
  //Agregamos los datos de profesionales. 
    nuevoObjeto[x.skill].users.push(
       x.user

    )
  	nuevoObjeto[x.skill].count+=1
})

console.log(nuevoObjeto)






# CASO 3
Tengo las siguientes funciones:
function buscarEnFacebook(texto, callback) {
  /* Hace algunas cosas y las guarda en "result" */
  if (result.error) {
    callback(error, result.error)
  } else {
    callback(null, result.data);
  }
}
function buscarEnGithub(texto, callback) {
  /* Hace algunas cosas y las guarda en "result" */
  if (result.error) {
    callback(error, result.error)
  } else {
    callback(null, result.data);
  }
}



1. ¿Qué debería hacer para usar la funcionalidad con promesas y no callbacks?

2. ¿Podés replicar la siguiente API?

buscador('hola')
.facebook()
.github()
.then((data) => {
  // data[0] = data de Facebook
  // data[1] = data de GitHub
})
.catch(error => {
  // error[0] = error de Facebook
  // error[1] de GitHub
})


y a la solución anterior

3. ¿Cómo podrías agregarle otra búsqueda?
4. ¿Cómo solucionas el problema de si una API entrega un error, mientras las otras devuelven data?
