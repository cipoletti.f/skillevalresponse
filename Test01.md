# Venta de BoXs

## Contexto y dominio
Tenemos una página de venta de BoxFee. Para cada BoXs hay varias tipos  y tamaños, en función de los cuales está el precio, por lo que al reservar una BoX el usuario debe elegir ambos. Por ejemplo, si el usuario elige tipo legacy, tamaño L  le sale $18,56, pero si lo elige para Standard mismo tamaño le sale $39,81.
Eventualmente el usuario a través del sitio confirma su reserva y paga, lo cual genera un ticket que es el comprobante de compra de la entrada.

1. Considerás, ¿estos requerimientos suficientes para proponer un esquema de desarrollo? Si no lo fueran, ¿que estarías solicitando al cliente?
2. ¿Podrás definir al menos 5 pruebas que permitan validar el cumplimiento de los requerimientos del cliente en los entregables?
3. Podrías si así fuere, indicar cuales son las omisiones en los requerimientos del cliente
4. Presentar de acuerdo al requerimiento propuesto, un modelado de datos y o diagrama de clases que refleje una primera aproximación al diseño
5. Por lo que se puede interpretar, ¿cuáles son los riesgos, los problemas ?
6. Presentar los diagramas de casos de uso
7. ¿En una análisis FODA, cuál o cuáles son las amenazas que podés detectar ?
8. ¿Para este caso planteado, es posible realizar un modelo de datos lógicos pensando en una solución orientada a objetos? ¿Que se debe considerar ?
9. ¿Podrás definir 4 casos de pruebas? 


Respuestas:

1. No. Dando por hecho que esta decidido por el equipo de desarrollo la tecnología o framework en la cual trabajar y donde se realiza el host, le preguntaría:
    ¿Debe hacerse una gestión de usuarios? ¿Que información solicitamos ingresar para registrarse?
    Para las reservas, ¿Por cuánto tiempo se guardan? ¿Las reservas vencen?. En el caso de que el precio del producto se actualice, ¿El precio se mantiene hasta que la reserva vence? 
    Los boxs, ¿Son finitos o ilimitados? 
    Para el cliente (dueño) de la página, ¿Necesitará de un usuario administrador para modificar precios y boxs? ¿Como visualizará las reservas / compras de los clientes?
    ¿Requerirá de algún reporte o información derivada de las compras? Ej: Cantidad de ventas, Boxs mas vendidos, usuarios que mas compran, etc. 
    ¿Por que medio se realizará el pago? ¿A que destino? ¿En que moneda? ¿Que datos tendrá el ticket? ¿El ticket se mandará al mail del cliente? ¿Se podrá descargar desde la misma página? ¿En qué formato?

2. Prueba 1. Añadir, eliminar y modificar producto (box) de la reserva.
    Prueba 2. Eliminar y comprar la reserva.
    Prueba 3. Registrar, modificar usuario.
    Prueba 4. Registrar precio de producto.
    Prueba 5. Iniciar y finalizar sesion de usuario.

3.  No dió una fecha en la cual espera tener el sistema finalizado.
    Omitió los posibles estados del pedido.
    Omitió la tecnología en la cual quiere su sistema y donde guardar la información. No indicó un plan de backup de la misma.
    No indicó aproximadamente la cantidad de usuarios esperados, necesario para determinar los recursos del host. 
    No indicó los pasos que debe seguir el cliente luego de comprar el box. No indicó información de contacto personal o de su empresa.
    
4. En repositorio
5. Concurrencia elevada de usuarios y servidor no preparado para la misma.
    Robo de datos e informacion. Metodos de pagos inseguros. 
6. En repositorio
7.  Aumento de la competencia. Aumento de paginas web para "crear" ecommerce pagando una suscripcion accesible. Regulaciones tributarias. Limitaciones del tipo de moneda posibles para recibir.
8. Si, se puede. Debería definir los atributos y su tipo de dato, claves primarias, foráneas y crear las clases intermedias donde sea necesario para almacenar la información.
9. 